import { trigger, state, stagger, style, transition, animate, query } from '@angular/animations';

export const showUpStraggered = trigger('showUpCollection',[
	transition('* => *',[
		query(':enter',[
			style({opacity: 0, transform:'scaleY(0)'}),
			stagger(70,[
				animate(300, style({ opacity: 1, transform: 'scaleY(1)' }))
				]),
			],{optional: true})
		])
	]);

export const showUp = trigger('showUpElement',[
	//Initial value, state = in
	state('in',style({ opacity: 1, transform: 'scaleY(1)' })),
	//Final value - state = void
	transition(':enter',[
		style({opacity: 0, transform:'scaleY(0)'}),
		animate(250)
		])
	]);